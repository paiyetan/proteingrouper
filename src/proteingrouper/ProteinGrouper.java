/*
 *
 *        Copyright (c) 2015, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 */
package proteingrouper;

import java.io.*;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import proteingrouper.database.Database;
import proteingrouper.mappers.PeptideProteinsMapper;
import proteingrouper.mappers.ProteinPeptidesMapper;
import proteingrouper.pepscan.PepScan;
import proteingrouper.pepscan.PepScanResult;
import utilities.OneDirChooser;
import utilities.OneFileChooser;

/**
 *
 * @author paiyeta1
 */
public class ProteinGrouper {

    private LinkedList<String> peptides = null;
    private LinkedList<String> proteins = null;
    private Database db = null;
    
    private HashMap<String, LinkedList<String>> protGroups;
    private LinkedList<String> peptidesAccFor;
    
    public ProteinGrouper(String pepsFile, String protsFile, String protsDB) throws FileNotFoundException, IOException {
        //throw new UnsupportedOperationException("Not yet implemented");
        System.out.println("Reading in input peptides/proteins...");
        InFileReader reader = new InFileReader(); 
        peptides = reader.readFile(pepsFile); //read in peptides from file..
        if(protsFile!=null)
            proteins = reader.readFile(protsFile); //read in proteins...
        
        System.out.println("Reading (and subsetting) protein database...");
        Database dtb = new Database(protsDB);//read in protein database...
        if(proteins==null){
            db = dtb;
        }else{
            db = dtb.subsetDatabase(proteins); //subset protein database...
        }
        
        System.out.println("Scanning peptides for protein occurrences in database...");
        PepScan pepscan = new PepScan(peptides,db);
        LinkedList<PepScanResult> pepScanResults = pepscan.getResults();
        
        //map peptide(s) to proteins
        System.out.println("Mapping peptide(s) to proteins...");
        PeptideProteinsMapper pepProtMapper = new PeptideProteinsMapper();
        HashMap<String, LinkedList<String>> pepToProtsMap = 
                pepProtMapper.mapPeptideToProteinAccns(pepScanResults); 
        //map protein(s) to peptides
        System.out.println("Mapping protein(s) to peptides...");
        ProteinPeptidesMapper protPepsMapper = new ProteinPeptidesMapper();
        HashMap<String, LinkedList<String>> protToPepsMap = 
                protPepsMapper.mapProteinAccnToPeptides(pepScanResults); 
        
        //filter-out proteins with less than a pre-specified number of peptides
        System.out.println("Filtering out proteins with < specified minimum peptide(s) requirement...");
        System.out.println("  Inintial Number of proteins: " + protToPepsMap.size());
        int removed = 0;
        /*
        Set<String> prots = protToPepsMap.keySet();
        for(String prot: prots){
            int mappedPeps = protToPepsMap.get(prot).size();
            if( mappedPeps < 2){ //default in this implementation to 
                                                      // minimum of peptides per protein... 
                protToPepsMap.remove(prot);
                removed++;
            }
        }
        * 
        */
        
        /*
        Iterator<Entry<String, LinkedList<String>>> i = protToPepsMap.entrySet().iterator();
        while(i.hasNext()){
            Entry entry = i.next();
            LinkedList<String> mappedPeps = (LinkedList)entry.getValue();
            if(mappedPeps.size() < 2){
                    i.remove(); //removes entry from map
                    removed++;
            }
        }
        * 
        */
        System.out.println("  Number of filtered out proteins: " + removed);
        //System.out.println("  " + removed + " proteins filtered out...");
        System.out.println("  Number of proteins to group: " + protToPepsMap.size());
        
        
        //group proteins
        System.out.println("Grouping proteins...");
        groupProteins(protToPepsMap, pepToProtsMap);
        System.out.println(" Merging/Removing redundant groups...");
        mergeRedundantProteinGroups();
        estimatePeptidesAccountedFor();
        
        System.out.println("  Number of protein groups: " + protGroups.size());
        System.out.println("  Number of peptides accounted for: " + peptidesAccFor.size());
        //return protein groups
        //
    }

    private void groupProteins(HashMap<String, LinkedList<String>> protToPepsMap, 
            HashMap<String, LinkedList<String>> pepToProtsMap) {
        //throw new UnsupportedOperationException("Not yet implemented");
        //Set<String> prots = protToPepsMap.keySet();
        Set<String> peps = pepToProtsMap.keySet();
        /* for each peptide (pep), assign to protein with the largest evidence 
           of presence (highest number of mapped peptides...)
           * 
           */
        protGroups = new HashMap<String, LinkedList<String>>();
        for(String pep: peps){
            LinkedList<String> mappedProts = pepToProtsMap.get(pep);
            LinkedList<String> potentialProtGrps = new LinkedList<String>(); //potential protein groups
                                                 // typically of lenght == 1, however, in a situation where a peptide
                                                 // maps to proteins with equally highest number of mapped peptides..
            
            // get highest number of mapped peptides from proteins to which peptide (pep) maps
            //int maxMappedPepOfMappedProts = protToPepsMap.get(mappedProts.getFirst()).size();
            int maxMappedPepOfMappedProts = 0;
            for(String mappedProt : mappedProts){
                // ensure protein still exist in the protein map (i.e. hadn't been previously filtered out)
                if(protToPepsMap.get(mappedProt)!=null){
                    if(protToPepsMap.get(mappedProt).size() > maxMappedPepOfMappedProts)
                        maxMappedPepOfMappedProts = protToPepsMap.get(mappedProt).size();
                }
            }
            
            // get prot(s) with highest number of mapped peptides...
            for(String mappedProt : mappedProts){
                // ensure protein still exist in the protein map (i.e. hadn't been previously filtered out)
                if(protToPepsMap.get(mappedProt)!=null){
                    if(protToPepsMap.get(mappedProt).size() == maxMappedPepOfMappedProts)
                        potentialProtGrps.add(mappedProt);
                }
            }
            
            // populate the master protein group...
            for(String potentialProtGrp : potentialProtGrps){
                if(protGroups.containsKey(potentialProtGrp)==false){
                    protGroups.put(potentialProtGrp, protToPepsMap.get(potentialProtGrp));
                }               
            }
        }
    }

    private void printProteinGroups(String output) throws FileNotFoundException {
        //throw new UnsupportedOperationException("Not yet implemented");
        PrintWriter printer = new PrintWriter(output);
        for(String protGroup : protGroups.keySet()){
            printer.println(protGroup);
        }
        printer.close();
    }

    private void printProteinGroupPeptides(String output) throws FileNotFoundException {
        //throw new UnsupportedOperationException("Not yet implemented");
        PrintWriter printer = new PrintWriter(output);
        printer.println("ProteinGroup\tMappedPeptides\tNumber");
        for(String protGroup : protGroups.keySet()){
            printer.println(protGroup + "\t" +
                            linkedListToString(protGroups.get(protGroup)) + "\t" +
                            protGroups.get(protGroup).size());
        }
        printer.close();
    }
    
    private String linkedListToString(LinkedList<String> strList) {
        //throw new UnsupportedOperationException("Not yet implemented");
        String str = "";
        for(int i = 0; i < strList.size(); i++){
            if((i < (strList.size() - 1)) && (i != (strList.size() - 1))){
                str = str + strList.get(i) + ", ";
            }
            if( i == (strList.size() - 1)){
                str = str + strList.get(i);
            }
        }
        
        return str;
    }

    private void estimatePeptidesAccountedFor() {
        //throw new UnsupportedOperationException("Not yet implemented");
        peptidesAccFor = new LinkedList<String>();
        Set<String> prots = protGroups.keySet();
        for(String prot : prots){
            LinkedList<String> peps = protGroups.get(prot);
            for(String pep : peps){
                if(peptidesAccFor.contains(pep)==false)
                   peptidesAccFor.add(pep);
            }
        }
    }

    private void mergeRedundantProteinGroups() {
        //throw new UnsupportedOperationException("Not yet implemented");
        LinkedList<String> grpsToRemove = new LinkedList<String>(); //list of flagged groups to remove/merge
        Set<String> prots = protGroups.keySet();
        String[] protsArray = new String[prots.size()];
        String[] protsArrayA = prots.toArray(protsArray);
        //int index = 0;
        //for(String p : protsArrayA){
        //    System.out.println("  " + index + ": " + p);
        //    index++;
        //}
        
        for(int i = 0; i < protsArrayA.length; i++){
            for(int j = 0; j < protsArrayA.length; j++){
                if( j > i){
                    LinkedList<String> protGrpImappedPeps = protGroups.get(protsArrayA[i]);
                    LinkedList<String> protGrpJmappedPeps = protGroups.get(protsArrayA[j]);
                    if(jIsSimilarToI(protGrpImappedPeps, protGrpJmappedPeps)){
                        //flag j for removal
                        grpsToRemove.add(protsArrayA[j]);
                    }
                }
            }
        }
        // remove redundant groups...
        for(String grp : grpsToRemove){
            protGroups.remove(grp);
        }
        
    }

    private boolean jIsSimilarToI(LinkedList<String> protGrpImappedPeps, LinkedList<String> protGrpJmappedPeps) {
        //throw new UnsupportedOperationException("Not yet implemented");
        boolean similar = true;
        if(protGrpImappedPeps.size()== protGrpJmappedPeps.size()){
            //check every peptide sequence..
            for(String pep : protGrpImappedPeps){
                if(protGrpJmappedPeps.contains(pep)==false){
                    similar = false;
                    break;
                }
            }
            
        }else{
            similar = false;
        }        
        return similar;
    }
    
    
    
    
    class InFileReader {

         //inputs;
        
        public LinkedList<String> readFile(String inputFile) throws FileNotFoundException, IOException {
            LinkedList<String> inputs = new LinkedList<String>();
            String line;
            BufferedReader reader = new BufferedReader(new FileReader(inputFile));
            while((line = reader.readLine())!=null){
                inputs.add(line);
            }
            reader.close(); 
            return(inputs);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        // TODO code application logic here
        String pepsFile = null;
        String protsFile = null;
        String protsDB = null;
        String outDir = null;
        
        
        System.err.println("Starting...");
        OneFileChooser chooser = new OneFileChooser("Load identified peptides list...");
        pepsFile = chooser.getInputFile();
        
        chooser = new OneFileChooser("Load mapped proteins list..."); // optional
        protsFile = chooser.getInputFile();
        
        chooser = new OneFileChooser("Load searched protein (Fasta) database...");
        protsDB = chooser.getInputFile();
        
        OneDirChooser dchooser = new OneDirChooser("Select output results directory...");
        outDir = dchooser.getInputFile();
        
        Date start_time = new Date();
        long start = start_time.getTime();
        
        ProteinGrouper pg = new ProteinGrouper(pepsFile,protsFile,protsDB);
        
        System.err.println("Printing protein groups...");
        pg.printProteinGroups(outDir + File.separator + "pDIdentifications.protgroups");
        pg.printProteinGroupPeptides(outDir + File.separator + "pDIdentifications.xprotgroups");
        
        System.out.println("\n...Done!!!");
        Date end_time = new Date();
        long end = end_time.getTime();
        System.out.println("End: " + end + ": " + end_time.toString());
        System.out.println("Total time: " + (end - start) + " milliseconds; " + 
                        TimeUnit.MILLISECONDS.toMinutes(end - start) + " min(s), "
                        + (TimeUnit.MILLISECONDS.toSeconds(end - start) - 
                           TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(end - start))) + " seconds.");
        
    }

    
}
