/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 * 
        Copyright (c) 2013, Paul Aiyetan
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 */
package proteingrouper.pepscan;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import proteingrouper.database.Database;
import proteingrouper.database.Peptide;
import proteingrouper.database.Protein;
//import sqldbextraction.database.Database;
//import sqldbextraction.database.Peptide;
//import sqldbextraction.database.Protein;

/**
 *
 * @author paiyeta1
 */
public class PepScan {    
    
    private LinkedList<PepScanResult> results;

    public PepScan(String pep2ScanFor, Database db) {
        //throw new UnsupportedOperationException("Not yet implemented");
        results = new LinkedList<PepScanResult>();
        scan(pep2ScanFor,db);
    }
    
    public PepScan(LinkedList<String> peptidesToScanFor, Database db) {
        //throw new UnsupportedOperationException("Not yet implemented");
        results = new LinkedList<PepScanResult>();
        Iterator<String> itr = peptidesToScanFor.iterator();
        int pepNumber = 0;
        while(itr.hasNext()){
            String pep2ScanFor = itr.next();
            pepNumber++;
            System.out.println("   Scanning database for instances of peptide " + pep2ScanFor);
            scan(pep2ScanFor,db);
        }       
    }
 
    private void scan(String pep2ScanFor, Database db) {
        //throw new UnsupportedOperationException("Not yet implemented");
        String peptideSequence = pep2ScanFor;
        LinkedList<Peptide> peptideInstances = new LinkedList<Peptide>();
        int proteinsScanned = 0;
        int peptideInstancesFound = 0;
        ArrayList<Protein> proteins = db.getProteins();
        Iterator<Protein> itr = proteins.iterator();
        
        while(itr.hasNext()){
            Protein protein = itr.next();
            if(protein.getSequence().contains(peptideSequence)){
                peptideInstancesFound++;
                Peptide peptide = protein.getPeptideInstance(peptideSequence);
                peptideInstances.add(peptide);              
            }
            proteinsScanned++;
            if(proteinsScanned%20000==0){
                System.out.println("    " + proteinsScanned + " proteins scanned...");
            }
            //if(peptideInstancesFound%10==0){
            //     System.out.println("     about " + peptideInstancesFound + " peptide Instances found for " + peptideSequence);
            //}
        }
        PepScanResult result = new PepScanResult(peptideInstances);
        results.add(result);
    }
    
    
    
    /*
    public void printScanResults(String output) {
        try {
            //throw new UnsupportedOperationException("Not yet implemented");
            PrintWriter printer = new PrintWriter(output);
            // print header
            printer.println("Peptide_sequence\t" +
                            "Sequence_with_neighbors\t" +
                            "Protein\t" +
                            "Position\t" +
                            "Percent_Protein_coverage");
            Iterator<PepScanResult> itr = results.iterator();
            while(itr.hasNext()){
                PepScanResult result = itr.next();
                LinkedList<Peptide> peptideInstances = result.getPeptideIntances();
                Iterator<Peptide> itr2 = peptideInstances.iterator();
                while(itr2.hasNext()){
                    Peptide peptide = itr2.next();
                    printer.println(peptide.getSequence() + "\t" +
                                    peptide.getSeqWithNeighbors() + "\t" +
                                    extractProteinName(peptide.getParent().getTitleLine()) + "\t" +
                                    peptide.getLocationString() + "\t" +
                                    peptide.getParent().getCoverage(peptide.getSequence()));
                }
            }
            printer.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PepScan.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }

    private String extractProteinName(String str) {
        
        char[] c = str.toCharArray();
        String title = null;
        for(int i = 0; i < c.length-8; i++){
           if(str.substring(i,i+6).matches("Tax.+")){
               title = str.substring(i);
               break;
           } 
        }
        //System.out.println(title+"\n");
        String[] titleArr = title.split("\\s");
        String name = titleArr[2]; //skip the Tax_Id=xxxxxx and Gene_Symbol=XXXXXX for ipi.v3.87 upwards
        for(int i = 3; i < titleArr.length; i++){ //re-append the rest of the line
            name = name + " " + titleArr[i];
        }
        //System.out.println(name);
        return name;
        
    }
    * 
    */

    public LinkedList<PepScanResult> getResults() {
        return results;
    }
}
