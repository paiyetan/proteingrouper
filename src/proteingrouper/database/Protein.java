/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 * 
        Copyright (c) 2013, Paul Aiyetan
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package proteingrouper.database;

import proteingrouper.database.enums.SearchDatabaseType;

/**
 *
 * @author paiyeta1
 */
public class Protein {
    
    private String titleLine;
    private String sequence;
        
    public Protein(String titleLine, String sequence){
        this.titleLine = titleLine;
        this.sequence = sequence;
    }
        
    public String getTitleLine() {
        return titleLine;
    }

    public String getSequence() {
        return sequence;
    }
    
    public String getReversedSequence(){
        char[] reverseStringArray = new char[sequence.length()];
        for(int i = sequence.length() - 1, j = 0; i != -1; i--, j++ ){
            reverseStringArray[j] = sequence.charAt(i);
        }
        return new String(reverseStringArray);
    }
    
    public double getCoverage(String peptideSequence){
        double coverage = 0;
        if(sequence.contains(peptideSequence)){
            int proteinLength = sequence.length();
            int peptideLength = peptideSequence.length();
            coverage = (double) peptideLength / (double) proteinLength * 100; 
        }
        
        return coverage;
    }
    
    public Peptide getPeptideInstance(String peptideSequence){
        Peptide peptide = null;
        // ensure peptide sequence is found in protein sequence
        if(sequence.contains(peptideSequence)){
            int startCharIndex = sequence.indexOf(peptideSequence);
            int endCharIndex = startCharIndex + (peptideSequence.length()-1);
            char nTermNeighbor;
            char cTermNeighbor;
            String seqWithNeighbors;
            // get nTerminal neighbor 
            if(startCharIndex > 0){
                nTermNeighbor = sequence.charAt(startCharIndex - 1);
            } else {
                nTermNeighbor = '-';
            }
            // get cTerminal neighbor
            if(endCharIndex < (sequence.length()-1)){
                cTermNeighbor = sequence.charAt(endCharIndex + 1);
            } else {
                cTermNeighbor = '-';
            }
            // assign sequence with neighbors
            seqWithNeighbors = nTermNeighbor + "." + peptideSequence + "." + cTermNeighbor; 
            peptide = new Peptide(peptideSequence, this, startCharIndex + 1, 
                                        endCharIndex + 1, seqWithNeighbors);
        }
        return peptide;
    }
    
    public String getAccession(SearchDatabaseType dbtype){
        String accn = "";
        switch(dbtype){
            
            default: //REFSEQ
                String[] headerArr = titleLine.split("\\|");
                //accn = "gi|" + headerArr[1]; 
                accn = headerArr[1]; 
        }
        return accn;       
    }
    
    public String getFullName(SearchDatabaseType dbtype){
        String fullName = "";
        switch(dbtype){
            
            default: //REFSEQ
                String[] headerArr = titleLine.split("\\|");
                fullName = headerArr[4];                    
        }
        return fullName;       
    }
    
}
